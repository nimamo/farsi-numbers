#Bidirectional conversion of Farsi/English numbers

To convert a number written in English symbols to a number written in Arabic or Farsi symbols or vise versa, it is enough only to replace each digit by its equivalent in other language. The mapping is as follows:

|English|Arabic/Farsi|
|:-:|:-:|
|0|۰|
|1|۱|
|2|۲|
|3|۳|
|4|۴|
|5|۵|
|6|۶|
|7|۷|
|8|۸|
|9|۹|

This package takes care of converting numbers appearing in a text in both directions. 
