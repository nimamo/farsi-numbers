<?php namespace nimaM;

class FarsiNumbers
{
    static $map = array(
        1 => "۱",
        2 => "۲",
        3 => "۳",
        4 => "۴",
        5 => "۵",
        6 => "۶",
        7 => "۷",
        8 => "۸",
        9 => "۹",
        0 => "۰",
    );

    static function toPersian($string)
    {
        foreach ( static::$map as $eng => $per )
            $string = str_replace($eng, $per, $string);
        return $string;
    }

    static function toEnglish($string)
    {
        foreach ( static::$map as $eng => $per )
            $string = str_replace($per, $eng, $string);
        return $string;
    }

}
